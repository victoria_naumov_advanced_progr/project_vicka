﻿#include "Room.h"

Room::Room(int id, User * admin, std::string name, int maxUsers, int questionNo, int questionTime) : _id(id) , _admin(admin) , _name(name) , _maxUsers(maxUsers) , _questionNo(questionNo) , _questionTime(questionTime)
{
	_users.push_back(_admin);
}

Room::~Room()
{

}

bool Room::joinRoom(User * user)
{
	bool check = false;
	string okMsg = "110";

	if (this->_maxUsers > _users.size())
	{
		_users.push_back(user);
		okMsg.append("0");
		okMsg.append(to_string(_questionNo));
		okMsg.append(to_string(_questionTime));

		user->send(okMsg);
		
		check = true;
	}
	else if(_users.size() == _maxUsers)
	{
		okMsg.append("1");
	}
	else
	{
		okMsg.append("2");
	}
	
	string message = getUserListMessage();
	sendMessage(message);
	

	return check;
}

void Room::leaveRoom(User * user)
{
	for (unsigned int i = 0; i <= _users.size(); i++)
	{
		if (_users[i] == user)
		{
			_users.erase(_users.begin() + i - 1);

			//הודעת עזיבה ליוזר שעזב
			
			string message = getUserListMessage();
			sendMessage(user, message);
		}
	}
}

int Room::closeRoom(User * user)
{
	int check = -1;
	if (user == _admin)
	{
		//הודעת סגירה לכל היוזרים
		string message;
		sendMessage(message);
	}
	
	for (unsigned int i = 0; i <= _users.size(); i++)
	{
		if (_users[i] != user)
		{
			_users[i]->clearRoom();
		}
	}

	return check;
}

vector<User*> Room::getUsers()
{

	return vector<User*>();
}

std::string Room::getUserListMessage()
{

	string message = "108";
	if (_users.size() != 0) //החדר קיים
	{
		string numOfUsers = to_string(_users.size());
		message.append(numOfUsers);

		for (unsigned int i = 0; i <= _users.size(); i++)
		{
			message.append("##");
			message.append(_users[i]->getUsername());
		}
	
	}
	else
	{
		message.append("0");
	}

	
	return message;
}

int Room::getQuestionsNo()
{

	return this->_questionNo;
}

int Room::getId()
{

	return this->_id;
}

std::string Room::getName()
{
	return this->_name;
}

std::string Room::getUsersAsString(vector<User*> usersList, User * excludeUser)
{

	return std::string();
}

void Room::sendMessage(std::string message)
{
	sendMessage(NULL, message);
}

void Room::sendMessage(User * excludeUser, string message)
{
	try
	{
		for (unsigned int i = 0; i <= _users.size(); i++)
		{
			if (_users[i] != excludeUser)
			{
				_users[i]->send(message);
			}
		}
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
}
