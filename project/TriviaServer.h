#pragma once

#include <thread>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include "RecievedMessage.h"
#include "User.h"
#include "Validator.h"


class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();

private:

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(RecvMessage* msg);

	User* handleSignin(RecvMessage*);
	void handleSignout(RecvMessage* msg);
	bool handleSignup(RecvMessage* msg);


	User* getUserBySocket(SOCKET client_socket);

	void handleRecievedMessages();
	void addRecievedMessage(RecvMessage*);
	RecvMessage* buildRecieveMessage(SOCKET userSock, int msgCode);

	SOCKET _socket;
	//MagshDocument _doc;

	// Queue for all clients. This way we will know who's the current writer.
	// SOCKET: client socket
	// string: userName
	//std::deque<pair<SOCKET, string>> _clients;
	std::map<SOCKET, User*> _connectedUsers;

	User* getUserByName(std::string username);
	std::map<std::string, std::string> signedUsers;


	// Queue for messages - Will hold the mssage code and the file data. To add messages use std::ref<const ClientSocket>
	// SOCKET: client socket
	// string: message
	//std::queue<RecvMessage*> _messageHandler;
	std::queue<RecvMessage*> _queRcvMessages;


	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;

	// Wake up when an action has been finished.
	//std::condition_variable _edited;

	//active rooms
	std::map<int, Room*> _roomsList;
	int static _roomidSequence;

	//DATABASE = _db;
};
