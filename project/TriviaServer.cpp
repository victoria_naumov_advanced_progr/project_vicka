#include "TriviaServer.h"
#include <exception>
#include <iostream>
#include <string>

// using static const instead of macros 
static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;


TriviaServer::TriviaServer()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

TriviaServer::~TriviaServer()
{
	//throw std::exception(__FUNCTION__ " - socket");
	// why is this try necessarily ?
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}


// listen to connecting requests from clients
// accept them, and create thread for each client
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}


void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();

}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	RecvMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(client_socket);

		while (msgCode != 0 && msgCode == 299)
		{
			currRcvMsg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = buildRecieveMessage(client_socket, 299);
		addRecievedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildRecieveMessage(client_socket, 299);
		addRecievedMessage(currRcvMsg);
	}
	//closesocket(client_socket);
}

void TriviaServer::addRecievedMessage(RecvMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}

RecvMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecvMessage* msg = nullptr;
	vector<string> values;
	if (msgCode == 200) // sign in
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);

		values.push_back(userName);
		values.push_back(password);
	}
	else if (msgCode == 201) //sign out
	{
		
	}
	else if (msgCode == 203)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);
		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);


		values.push_back(userName);
		values.push_back(password);
		values.push_back(email);

	}


	msg = new RecvMessage(client_socket, msgCode, values);
	return msg;

}


User * TriviaServer::getUserBySocket(SOCKET client_socket)
{
	std::map<SOCKET, User*>::iterator itConUsers = _connectedUsers.find(client_socket);
	if (itConUsers != _connectedUsers.end())
	{
		return itConUsers->second;
	}
	return nullptr;
}

void TriviaServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	User* currUser = nullptr;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecvMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();

			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			currUser = getUserBySocket(clientSock);
			currMessage->setUser(currUser);


			msgCode = currMessage->getMessageCode();

			if (msgCode == 200) 
			{
				this->handleSignin(currMessage);
			}
			else if (msgCode == 203)
			{
				this->handleSignup(currMessage);
			}
		}
		catch (...)
		{

		}
	}
}



User* TriviaServer::handleSignin(RecvMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	
	std::map<string, string>::iterator itSignedUsers = this->signedUsers.find(username); // האם הוא נמצא ב DB
	if (itSignedUsers == this->signedUsers.end())
	{
		// there no signed user with this username	
		Helper::sendData(_socket,"1021");
		//הודעת כישלון
		return nullptr;

	}
	if (itSignedUsers->second == password)
	{
		if (getUserByName(username))
		{
			Helper::sendData(_socket, "1022");
			//הודעת כישלון

		}
		else
		{
			User* newUser = new User(username, _socket);
			Helper::sendData(_socket, "1020");
			//הודעת התחברות מוצלחת
		}
	}
	return nullptr;
}

void TriviaServer::handleSignout(RecvMessage* msg)
{
	
}

bool TriviaServer::handleSignup(RecvMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string mail = msg->getValues()[2];


	if (!Validator::isPasswordValid(password))
	{
		Helper::sendData(_socket, "1041");
		return false;
	}
	if (!Validator::isUsernameValid(username))
	{
		Helper::sendData(_socket, "1043");
		return false;
	}

	std::map<string, string>::iterator itUser = signedUsers.find(username);
	if (itUser != signedUsers.end())
	{
		//כישלון
		Helper::sendData(_socket, "1042");
		return false;
	}

	std::pair<string, string> p(username, password);
	signedUsers.insert(p);
	Helper::sendData(_socket, "1040");
	return true;


}


// remove the user from queue

//needs to be changed _connectedUsers!!!!

void TriviaServer::safeDeleteUser(RecvMessage* msg)
{
	try {
		SOCKET sock = msg->getSock();
		// this->handlesignout();
		::closesocket(sock);
	}
	catch (exception e) {
		cout << e.what();
	}
}


User * TriviaServer::getUserByName(std::string username)
{
	std::map<SOCKET, User*>::iterator itConUsers;
	for(itConUsers = _connectedUsers.begin(); itConUsers != _connectedUsers.end(); itConUsers++)
	{
		if (itConUsers->second->getUsername() == username)
		{
			return itConUsers->second;
		}
	}
	
	return nullptr;
}


