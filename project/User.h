#pragma once

#include "ServerHelper.h"
#include <thread>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include "RecievedMessage.h"
#include "Helper.h"
#include "Room.h"

class Room;

class User
{
public:
	User(std::string username, SOCKET sock);
	~User();

	void send(std::string message);

	string getUsername();

	SOCKET getSocket();
	
	Room* getRoom();

	//Game* getGame();

	//void setGame(Game* gm);


	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime);

	bool joinRoom(Room* newRoom);

	void leaveRoom();

	int closeRoom();

	//bool leaveGame();

	void clearRoom();



private:
	std::string _username;
	Room* _currRoom;
	//Game* _currGame;
	SOCKET _sock;


};
