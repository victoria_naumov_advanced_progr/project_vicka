#pragma once

#include <string>
#include <vector>
#include <WinSock2.h>
#include <Windows.h>
#include "User.h"

using namespace std;

class User;
class Room;

class RecvMessage
{
public:
	RecvMessage(SOCKET sock, int messageCode);

	RecvMessage(SOCKET sock, int messageCode, vector<string> values);

	SOCKET getSock();
	int getMessageCode();
	User* getUser();
	void setUser(User* user);

	vector<string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};
