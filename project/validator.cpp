#include "Validator.h"

bool Validator::isPasswordValid(std::string password)
{
	int i = 0;
	char k;
	if (password.length() >= 4)
	{

		if (password.find_first_of("0123456789") == std::string::npos)
		{
			for (i = 0; i < password.length(); i++)
			{
				k = password.at(i);
				if (isspace(k))
				{
					return false;
				}
				if (isupper(k) || islower(k))
				{
					return true;
				}
				//i++
			}
		}
	}
	return false;
}
