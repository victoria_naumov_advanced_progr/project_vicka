#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include "RecievedMessage.h"
#include "Helper.h"
#include "User.h"

using namespace std;

class User;
class RecvMessage;

class Room
{
public:
	Room(int id, User* admin, std::string name, int maxUsers, int questionNo, int questionTime);
	~Room();


	bool joinRoom(User* user);

	void leaveRoom(User* user);

	int closeRoom(User* user);

	vector <User*> getUsers();

	std::string getUserListMessage();

	int getQuestionsNo();

	int getId();
	
	std::string getName();
	
	

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	std::string _name;
	int _id;



	std::string getUsersAsString(vector<User*> usersList, User* excludeUser);
	void sendMessage(std::string message);
	void sendMessage(User* excludeUser, string message);


};
