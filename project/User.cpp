﻿#include "stdafx.h" 
#include "User.h"

User::User(std::string username, SOCKET sock) : _username(username) , _sock(sock)
{
	//this->_currGame = nullptr;;
	this->_currRoom = nullptr;
}

User::~User()
{

}

void User::send(std::string message)
{
	Helper::sendData(this->_sock, message);
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room * User::getRoom()
{
	return this->_currRoom;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		this->send("1141");
		return false;
	}
	_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	this->send("1140");
	return true;
}

bool User::joinRoom(Room * newRoom)
{
	bool check = false;
	if (this->_currRoom != nullptr)
	{
		return check;
	}
	
	check = newRoom->joinRoom(this); 
	if (check == true)
	{
		this->_currRoom = newRoom;
	}
	return check;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this); 
		this->_currRoom = nullptr; 
	}
}

int User::closeRoom()
{
	int check = -1;
	if (!_currRoom)
	{
		return -1;
	}

	check = _currRoom->closeRoom(this);
	if (check != -1)
	{
		delete _currRoom;
		this->_currRoom = nullptr;

	}
	return check;
}


void User::clearRoom()
{
	this->_currRoom = nullptr;
}
