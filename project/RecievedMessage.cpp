
#include "RecievedMessage.h"

RecvMessage::RecvMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

RecvMessage::RecvMessage(SOCKET sock, int messageCode, vector<string> values) : RecvMessage(sock, messageCode)
{
	_values = values;
}

SOCKET RecvMessage::getSock()
{
	return _sock;
}


int RecvMessage::getMessageCode()
{
	return _messageCode;
}

User * RecvMessage::getUser()
{
	return this->_user;
}

void RecvMessage::setUser(User * user)
{
	this->_user = user;
}


vector<string>& RecvMessage::getValues()
{

	return _values;
}
